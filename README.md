[![pipeline status](https://gitlab.com/dazach/ostfalia-cup-website/badges/master/pipeline.svg)](https://gitlab.com/dazach/ostfalia-cup-website/-/commits/master)

---

## Setup

To start this GitLab Instanz with a integrated runner + this repository already installed runn the following
```bash
git clone https://gitlab.com/dazach/ostfalia-cup-website.git
cd ostfalia-cup-website/docker
docker build -t gitlab-oc .
chmod +x run.sh
./run.sh
```

To set the Wildcard Domain do the following:
Place the certificate and key inside /etc/gitlab/ssl
In /etc/gitlab/gitlab.rb specify the following configuration:
```bash
pages_external_url 'https://example.io'

pages_nginx['redirect_http_to_https'] = true
pages_nginx['ssl_certificate'] = "/etc/gitlab/ssl/pages-nginx.crt"
pages_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/pages-nginx.key"
```
sudo gitlab-ctl reconfigure

Admin-user: root
Password: 1.0_pls!


## GitLab CI

This project's static pages are built by the the GitLab CI pipeline, following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).


## Using Jekyll locally

To work locally with this project, you'll have to follow the steps below:

1. Install [Ruby](https://www.ruby-lang.org/en/downloads/)
2. Clone the repository
```bash
git clone https://gitlab.com/dazach/ostfalia-cup-website.git
```
3. Install dependencies
```bash
cd ostfalia-cup-website
gem install bundler
bundle install
```
4. Run local server with live reload
```bash
bundle exec jekyll serve --livereload
```
