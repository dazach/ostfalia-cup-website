---
short_name: Nils
name: Nils Tegetmeyer
studiengang: Informatik M.Sc.
vertiefung: Mobile System Engineering
aufgaben: Bildverarbeitung
img: /assets/images/nofoto.webp
---
Ich finde es sehr interessant im Automotivebereich mich in
Richtung des autonomen Fahrens weiterzubilden und in einem motivierten
Team Probleme zu lösen.