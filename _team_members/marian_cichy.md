---
short_name: Marian
name: Marian Cichy
studiengang: Informatik
vertiefung: Computer Engeneering
aufgaben: Linux, Systemarchitektur, Projektleitung
img: /assets/images/nofoto.webp
---
Marian liebt die Möglichkeit, an einem großen Projekt fundamental mitzuarbeiten und eigene Ideen einbringen zu können.