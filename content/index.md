---
layout: index
title: Startseite
permalink: /index

abschnitte:
        - headline: Was ist der Ostfalia-Cup?
          background: clouds.jpg
          scheme: dark
          text: Der Ostfalia Cup ist eine Arbeitsgemeinschaft, die seit August 2008 autonom fahrende Fahrzeuge im Maßstab 1:10, nach dem Vorbild der DARPA Grand Challenge entwickelt. Die Fahrzeuge verfügen über eine Fahrbahn- und Objekterkennung mit Hilfe von Kamera und Sensoren, die eine Hindernis- und Kreuzungserkennung, sowie eine Parklückennavigation ermöglicht.

        - headline: Was bietet der Ostfalia-Cup?
          background: black_2.jpg
          scheme: light
          parallax: true
          text: Der Ostfalia-Cup bietet den Studenten die Möglichkeit erlerntes Wissen aus dem Studium anzuwenden und zu vertiefen. Gerne betreut das Ostfalia-Cup-Team studentische Projekte, die sich u.U. beim Studium anrechnen lassen. Beim Ostfalia-Cup fallen Arbeiten in allen möglichen Bereichen an, von C-Programmierung für Anwendungen und μ – Prozessoren über Entwicklung von eigenen Platinen bis hin zur modellbasierten Entwicklung von Software sowie das Testen der Hardware...
---



