---
layout: fahrzeuge
title: Fahrzeuge
permalink: /fahrzeuge
---

{::options auto_ids="false" /}

{:.uk-heading-line .uk-text-center}

# Was zeichnet die Fahrzeuge aus?

Die Informationen, die die einzelnen Sensoren ermitteln, werden im Mikrocontroller (Programmiersprache C) verarbeitet. Aus den gelieferten Werten werden Handlungsanweisungen erstellt und an die einzelnen Akteure wie z.B. Blinker, Lenkung und Motor ausgegeben.

Im einen eigens entwickelten Simulator werden die Bilder der realen Welt unter Berücksichtigung der physikalischen und technischen Randbedingungen simuliert. Dies ermöglicht es die Algorithmen zur Fahrstreckenplanung und Objekterkennung zu Testen.

{:.uk-heading-line .uk-text-center}

# Fahrzeug Simulation

Der unter Java entwickelte Simulator ermöglicht es nahezu in Echtzeit Sensorik, Videokamera und die Aktorik zu simulieren, um Algorithmen der Fahrbahnerkennung und Fahrstreckenplanung zu testen. Mittels einer C-Schnittstelle können alle erdenklichen Szenarien ohne Hardware realistisch nachgebildet werden. Der Simulator ist daher in der Lage, die Hardwareebene des Fahrzeuges zu simulieren.

{:.uk-heading-line .uk-text-center}

# Modelbasiertes entwickeln

Mit Hilfe von Merapi Modelling lassen sich modellbasiert C-Code für das Fahrzeug aus Diagramen und State-Charts erzeugen.
