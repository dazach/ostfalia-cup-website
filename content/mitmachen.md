---
layout: mitmachen
title: Mitmachen
permalink: /mitmachen
themen:
    - name: C/C++-Programmierung
      icon: code
    - name: Linux
      icon: happy
    - name: Embedded Programmierung
      icon: code
    - name: Modellbasierte Softwareentwicklung
      icon: database
    - name: Elektrotechnik
      icon: bolt
    - name: KI
      icon: social
    - name: Bildverarbeitung
      icon: image
    - name: Projektmanagement
      icon: calendar
    - name: Regelungstechnik
      icon: settings
    - name: Rechnerstrukturen
      icon: cog
    - name: Verteilte- und Vernetzte Systeme
      icon: cloud-upload
    - name: Netzwerkmanagement (CAN, I2C, Ethernet)
      icon: server
    - name: Simulation (ADTF & VTD)
      icon: future
    - name: Fahrerassistenzsysteme
      icon: lifesaver
    - name: Testmanagement
      icon: check
---

{::options auto_ids="false" /}

{:.uk-heading-line .uk-text-center}

# Mitmachen

Unser kleines Carolo-Cup Team bietet die perfekte Umgebung um viele
neue Eindrücke zu sammeln!
Bei uns kannst du Team- und Projektarbeit erleben, mitwirken und etwas
bewegen (in erster Linie unser [kleines Auto]). Die Arbeit im Ostfalia-Cup
ist vielseitig. In vielen Bereichen kannst du dein Wissen vertiefen, dir
neues Wissen aneignen und deine Fähigkeiten gewinnbringend
einbringen.

Egal ob du lieber programmierst oder doch lieber an Hardware tüftelst, hier findest du etwas was dir liegt und dich interessiert, vielleicht findest du auch deine neue Leidenschaft!
Du bist herzlich eingeladen uns zu besuchen und anzuschließen!
