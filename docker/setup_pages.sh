#!/bin/bash
echo '127.0.0.1       pages.example.com root.pages.example.com *.example.com' >> /etc/hosts
(echo 'pages_external_url "http://pages.example.com/"';echo "gitlab_pages['enable'] = true"; echo "gitlab_pages['listen_proxy']" '= "localhost:8090"'; echo "gitlab_pages['inplace_chroot'] = true") >> /etc/gitlab/gitlab.rb
gitlab-ctl reconfigure
echo "<!-- It's over, it's done -->" >> /home/ostfalia-cup-website/_layouts/index.html
cd /home/ostfalia-cup-website || return
git config --global user.email "root@example.com"
git config --global user.name "root"
git add .
git commit -m "Unleash the Pipeline"
git push --set-upstream http://root:1.0_pls!@localhost/root/root.pages.example.com.git master

